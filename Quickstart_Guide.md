# Quickstart Guide for setting up Vitis / Vivado 2022.1 for U250

## VM and Vitis Setup
1. Install VirtualBox for your machine. https://www.virtualbox.org/
2. Install "ubuntu-18.04.02-desktop-amd64.iso" from http://old-releases.ubuntu.com/releases/18.04.2/
3. In VirtualBox click "New". Name the machine Ubuntu180402. For the folder choose your Desktop. For ISO Image select the .iso from Step 2.
4. For Unattended Guest OS choose any username and password.
5. For Hardware select at least 16gbs for RAM and at least 4 processors. It is recommended to go to the largest amount on the green slider to maximize compile times.
6. Create a virtual hard disk with 275gb of space.
7. Boot up the machine. This will take a few minues.
8. Power off the machine. Under the settings tab in VirtualBox select Display. Change the Graphics Controller to VBoxVGA. Power the VM up again.
9. Open up a terminal and get into root.
```css
su -c
```
10. Add your username to sudoers file.
```css
gedit /etc/sudoers
```
Add this line to the bottom of the file (Make sure to replace username with your actual username):
```css
username  ALL=(ALL) NOPASSWD:ALL
```
Type exit to get out of root

11. Open up FireFox in the VM and visit this link: https://www.xilinx.com/support/download/index.html/content/xilinx/en/downloadNav/vitis/2022-1.html

Select the 2022.1 Version and Download "Xilinx Unified Installer 2022.1 Linux Self Extracting Web Installer

If you do not have an AMD account create one with your Illinois email.

12. In the terminal under Downloads allow permissions to file:
```css
chmod +x Xilinx_VERSIONxxxx.bin
```

13. Execute the download script:
```css
./Xilinx_VERSIONxxx.bin
```

14. Under devices deselect the Versal ACAP option, this will help reduce the space needed for install.

15. Change the installation directory to a new directory in your home directory. Click Install. This will take many hours!


## U250 Setup

1. Visit this link through FireFox in the VM: https://www.xilinx.com/products/boards-and-kits/alveo/u250.html#gettingStarted

2. Go to the "Getting Started" tab and select 

2022.1

Gen3x16-XDMA

x86_64

Ubuntu

18.04

3. Download the XRT file and allow permissions in downloads folder:
```css
chmod +x xrt_VERSION.deb
```
```css
sudo apt install ./xrt_VERSION.deb
```
4. Download the Deployment Target Platform 

5. In your directory that holds all the files:
```css
mkdir Deployment

cd Deployment
```
```css
mv ~/Downloads/xilinx-u250-gen3x16-VERSION.deb.tar.gz . 
```
```css
chmod +x xilinx-u250-gen3x16-VERSION.deb.tar.gz

tar -xvzf xilinx-u250-gen3x16-VERSION.deb.tar.gz

sudo apt install ./*.deb
```

6. Set up the environment:
```css
source /opt/xilinx/xrt/setup.sh
```
```css
cd xilinxDir/Vitis/2022.1/scripts

sudo ./installLibs.sh
```

7. Download the Development Target Platform from the Xilinx website

8. In your directory that holds all the files:
```css 
mkdir Platform

cd Platform
```
```css
mv ~/Downloads/xilinx-u250-gen3x16-xdma-VERSION.deb .

chmod +x xilinx-u250-gen3x16-xdma-VERSION.deb

sudo apt install ./xilinx-u250-gen3x16-xdma-VERSION.deb .
```

9. Set the platform path:
```css
export PLATFORM_REPO_PATHS=<path to Platform directory>
```

10. Start Vitis:
```css 
cd Vitis/2022.1

source settings64.sh

vitis .
```

11. Create application project, click next. You should now see the U250 as one of the board options to create a project with!

12. Name the project U250_first and use the Simple Vector Addition and click finish.



## Vivado setup
1. Download board and XDC files from U250 product page

2. Extract the board files to Vivado/2022.1/data/xhub/boards/XilinxBoardStore/boards/Xilinx

3. Unzip the XDC file in an easy to access directory

4. Source settings.sh in Vivado/2022.1

5. Run Vivado 

6. When creating a project choose the XDC files and under board you should see the U250 listed



