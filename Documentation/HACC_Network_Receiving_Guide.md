# HACC Networking Receiving Guide

## Receiving on NIC

The NIC is an interface between the transmission from the master 100gb/s switch and an actual compute node.
#

## Steps to network capture on NIC with tcpdump: (No longer able to be used 4/17 due to security concerns)
1. ssh into a free login node as described in the user guide
2. View which interfaces are able to be captured on with: 
```css
tcpdump -D
``` 
3. Create a script for SLURM to network capture (more details in SLURM document)
4. Set up a network capture with:
```css
tcpdump -i <NIC name> -w <filename.pcap>
```
#
## Steps to network capture on NIC with an IP address with python scripting:
1. ssh into a free login node as described in the user guide
2. Create a python script that is able to receive UDP packets as described as:
```css
import socket
from datetime import datetime
import time

UDP_IP = " "
UDP_PORT = x
BUF_SIZE = x
RUNTIME = x # Capture time in seconds

time = datetime.now()
stamp = time.strftime(%Y_%m_%d_%H_%M_%S)
fname = f"UDP_output_{stamp}.txt"

sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
sock.bind((UDP_IP, UDP_PORT))

with open(fname, 'w') as f:

start_time = time.time()
elapsed_time = 0
while elapsed_time < RUNTIME:
    data, addr = sock.recvfrom(BUF_SIZE)
    f.write(data)
    elapsed_time = time.time() - start_time
```
adapted from https://wiki.python.org/moin/UdpCommunication

.recvfrom() is a socket function that returns the data object as well as the hosts IP address and port

3. Create a SLURM script to execute the python script as a job - look in SLURM guide
   
#
## Steps to network capture on NIC with a MAC address with python scripting:
1. ssh into a free login node as described in the user guide
2. Create a python script that is able to receive UDP packets as described as:

```css
import socket
import struct
import time
from datetime import datetime

mac_address = 

RUNTIME = 10
time = datetime.now()
stamp = time.strftime(%Y_%m_%d_%H_%M_%S)
fname = f"NIC_output_{stamp}.txt"


sock = socket.socket(socket.AF_PACKET, socket.SOCK_RAW, socket.htons(0x0003))
sock.bind(('eth0', 0))

with open(fname, 'w') as f:

start_time = time.time()
elapsed_time = 0
while elapsed_time < RUNTIME:

    packet, address = sock.recvfrom(65535)

    eth_header = packet[:14]
    eth_src = eth_header[6:12]

    if eth_src == mac_address:
        f.write(data)

    elapsed_time = time.time() - start_time
```
3. Create a SLURM script to execute the python script as a job - look in SLURM guide


# Steps for networking to FPGA (WIP)
adapted from https://xilinx-center.csl.illinois.edu/xacc-cluster/xacc-user-guide/xacc-cluster-xacc-user-guide-xacc-networked-fpga-guide/

The Xilinx U280 is based on the Xilinx UltraScale+ architechture and is optimized for high-speed transceiving
Development MUST be done on Vitis 2021.1

AMD Guide on Transceiver Development: https://www.xilinx.com/developer/articles/designing-a-transceiver-based-application-with-vitus.html 

U280 Guide: https://docs.xilinx.com/r/en-US/ug1301-getting-started-guide-alveo-accelerator-cards

UltraScale+ Devices Integrated 100G Ethernet Subsystem Product Guide: https://docs.xilinx.com/r/en-US/pg203-cmac-usplus/Overview?tocId=DOOF49KeZwgUAuowZIwnKw
https://www.xilinx.com/content/dam/xilinx/support/documents/ip_documentation/cmac_usplus/v3_1/pg203-cmac-usplus.pdf

10gb Ethernet Subsystem Guide: https://www.xilinx.com/products/intellectual-property/ef-di-25gemac.html

An Ethernet port is connected directly to the U280 in the HACC Cluster

#

## VNx guide
VNx is used to communicate up to 100 gb/s UDP on the Xilinx 250 and 280 (this may be unnecessary w 100G ethernet subsystem)

Repository: https://github.com/Xilinx/xup_vitis_network_example



https://xilinx.entitlenow.com/AcrossUser/main.gsp?product=0452533&tab=&req_hash=&



## Design Guide (WIP):

1. Configure the 100G Ethernet Subsystem IP core with the Vivado IP Integrator tool

2. Connect the 100G Ethernet Subsystem to the U280 FPGA fabric using AXI interfaces. The AXI interfaces allow the Ethernet subsystem to communicate with other IP cores in the FPGA.

3. Use the AXI DMA IP core to transfer data from the Ethernet subsystem to the U280's DDR memory. The AXI DMA core provides high-speed data transfer between memory and the Ethernet subsystem, allowing the FPGA to receive data at 10 Gb/s.

4. Create tickerplant logic

5. Verify

