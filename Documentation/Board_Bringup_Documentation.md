# U280 Board Bringup Documentation

## WIP Notes: just raw steps rn will add detailed notes later

## for local dev

1. Download VirtualBox 
2. Spin up Ubuntu18.04 VM with 18.04.2.iso http://old-releases.ubuntu.com/releases/18.04.2/ 
3. Set RAM to at least 16gb and storage to at least 250gb
4. Change graphics controller to VBoxVGA 
5. Add root permissions with: su -
usermod -a -G sudo USERNAME
1. Download Ubuntu 18.04
2. chmod
3. ./
4. Install Xilinx Vitis
5. sudo apt-get install libswt-gtk-4-java (re)
6. Download Xilinx runtime
7. sudo apt install ./xrt_<version>.deb
8. Download Xilinx target platform
9. mkdir Deployment
10. Move into Deployment
11. sudo apt install ./*.deb
12. source /opt/xilinx/xrt/setup.sh
13. sudo installLibs.sh in /Vitis/<release>/scripts/
14. Download U280 development platform from website
15. mkdir Platform
16. mv download into Platform
17. sudo apt install file
18. export PLATFORM_REPO_PATHS=<path to platforms>
19. sudo apt-get install ocl-icd-lib
20. sudo dpkg -i xrtfilename.deb and U280 files
21. Source settings.sh
22. vitis



## To obtain 100gb Ethernet Subsystem 
1. Generate node locked license from https://www.xilinx.com/products/intellectual-property/cmac_usplus/cmac_usplus_order.html
2. vlm
3. find Host Name
4. host id is your mac address
5. ip addr show
6. link/ether field
7. Download .lic file
8. back in vlm, load the .lic file in


## Pynq setup
1. Install anaconda
2. wget https://repo.anaconda.com/archive/Anaconda3-2022.05-Linux-x86_64.sh -O anaconda3.sh
bash anaconda3.sh
3. allow your conda environment to startup on terminal open
4. conda install -c conda-forge jupyterlab
5. pip install pynq
   

# Vivado Guide (Jason's Recommendation):
https://docs.xilinx.com/r/en-US/ug1314-alveo-u280-reconfig-accel/Vivado-Design-Flow

Vivado Board Support and XDC Files Installation

Import the 100gb subsytem as IP block


https://docs.xilinx.com/r/2021.1-English/ug1393-vitis-application-acceleration/Execution-Model