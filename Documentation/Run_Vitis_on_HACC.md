

#### To run a Vitis project made on a local machine on a High-Performance Computing (HPC) cluster

1. Ensure compatibility: Make sure that the HPC cluster you are planning to use has the required hardware, such as Xilinx FPGA devices, and software, such as the Vitis development environment, to run your project.
2. Transfer files to the HPC cluster: Use a secure file transfer method like SCP (Secure Copy Protocol) or SFTP (SSH File Transfer Protocol) to transfer your project files from your local machine to the HPC cluster. For example:

```
scp -r /path/to/your/vitis_project username@hpc_cluster_address:/path/to/destination_folder
```

3. Configure the environment: Log in to the HPC cluster, usually via SSH, and set up the necessary environment variables for the Vitis development environment. You may need to load the Vitis module if it is managed by a module system like Environment Modules or Lmod.

```
ssh username@hpc_cluster_address
module load vitis
```

4. Compile and build: Go to the directory where you transferred your project and compile/build the Vitis project.

```
cd /path/to/destination_folder/vitis_project
make all
```

5. Create a job script: Create a job script to run your Vitis project on the HPC cluster. Job scripts vary depending on the workload manager in use (e.g., SLURM, PBS Pro, or LSF). Here's a basic example of a SLURM job script:

```
#!/bin/bash
#SBATCH --job-name=your_job_name
#SBATCH --ntasks=1
#SBATCH --cpus-per-task=4
#SBATCH --mem=16G
#SBATCH --time=01:00:00
#SBATCH --partition=your_partition_name
#SBATCH --gres=fpga:1

module load vitis
cd /path/to/destination_folder/vitis_project

# Execute your Vitis application
./your_vitis_executable
```

6. Submit the job: Submit the job script to the HPC cluster using the appropriate command for the workload manager. For example, if you are using SLURM, you would use `sbatch`.

```
sbatch your_job_script.sh
```

7. Monitor job progress: Monitor the progress of your job using the appropriate command for the workload manager. For example, if you are using SLURM, you can use the `squeue` command.

```
squeue -u username
```

8. Retrieve results: Once the job has completed, you can retrieve the results from the HPC cluster by transferring the output files back to your local machine using SCP or SFTP.

These are the general steps to run a Vitis project on an HPC cluster. Note that some details may vary depending on the specific cluster configuration and workload manager. Consult your HPC cluster documentation for any specific requirements or procedures.
