# Intro to HACC Cluster 

## Nevigate to the cluster

​	Firstly, the HACC Cluster is a high-performance computing (HPC) system that uses the Linux operating system. It is designed to support high-performance computing (HPC), machine learning (ML), and genomics applications, and for investigating programming models, compilers, schedulers, systems and architectures. It is scalable and expandable, and encompasses modern FPGAs, multi-GPU nodes, near-memory acceleration devices, and emerging compute devices. To access the HACC Cluster, you will need an account and access credentials provided by your institution's HPC administrator.

Before you have a the account, make sure you’ve completed the following:

1. XACC user survey form.
2. Signed external user agreement forms.
3. Familiarized yourself with the linux command line.

To request access to XACC, please contact the the xilinx-center. Upon completion of the above checklist, the system admin will provide you with an XACC account.



​	Once you have access to the HACC Cluster, you can log in via the command line interface (CLI) using a Secure Shell (SSH) client or login the web-client. To do this, open a terminal on your local machine and enter the following command:

```css
ssh <username>@hacc.rit.albany.edu ##username@hostname
```

If you are a UIUC user, you can use the follwing hostname

```css
ssh <username>@xacc-head-000-5.csl.illinois.edu
```

Replace `<username>` with your HACC Cluster username.

On the web-client, you can have a web-based terminal to interact with the cluster and the client provides you with access to upload/edit/download files.

Please use direct ssh with your hostname for first time log in to create the home directory successfully.

When you log into the XACC cluster for the first time make sure to do the following:

1. Update your password from the provided default.

2. Ensure that a home directory has been created for you. If you do not have a home directory, contact the system admin.

   

The XACC cluster has been designed for Xilinx Alveo cards as the primary accelerators. Thus, the programming and execution model has been setup to enable easy and reliable access to the FPGA accelerators. There are several different cluster machines with varied resources. Note that only some machines are open to external users.  For security, isolation, and ease of management HACC have virtualized the cluster machines. 

For a list of all available nodes (VM and non-VM) nodes, please check the [cluster status page.](https://xilinx-center.csl.illinois.edu/xacc-cluster/hacc-cluster-status/)The cluster status page will be periodically updated to reflect changes in node status, availability, and resources.

#### Command line:

Once you have logged in, you will be in your home directory. To navigate through the HACC Cluster, you will need to use the command line interface. Here are some basic commands that you can use to navigate through the file system:

1. `cd`: Change directory

   ```bash
   cd <directory>
   ```

   Replace `<directory>` with the name of the directory you want to navigate to. For example, `cd Documents` will navigate to the "Documents" directory.

2. `ls`: List directory contents

   ```bash
   ls <options> <directory>
   ```

   Replace `<directory>` with the name of the directory whose contents you want to list. The `<options>` parameter can be used to modify the behavior of the `ls` command. For example, `ls -l` will list the contents of the current directory in long format.

3. `pwd`: Print working directory

   ```bash
   pwd
   ```

   This command will print the current working directory.

4. `mkdir`: Make directory

   ```bash
   mkdir <directory>
   ```

   Replace `<directory>` with the name of the directory you want to create. For example, `mkdir Documents` will create a new directory named "Documents".

5. `rm`: Remove file

   ```bash
   rm <file>
   ```

   Replace `<file>` with the name of the file you want to remove. For example, `rm file.txt` will remove a file named "file.txt".

6. `rmdir`: Remove directory

   ```bash
   rmdir <directory>
   ```

   Replace `<directory>` with the name of the directory you want to remove. For example, `rmdir Documents` will remove a directory named "Documents". Note that the directory must be empty before it can be removed.

7. `cp`: Copy file

   ```bash
   cp <source> <destination>
   ```

   Replace `<source>` with the name of the file you want to copy, and `<destination>` with the name of the destination file. For example, `cp file.txt Documents/file.txt` will copy a file named "file.txt" to the "Documents" directory.

8. `mv`: Move file

   ```bash
   mv <source> <destination>
   ```

   Replace `<source>` with the name of the file you want to move, and `<destination>` with the name of the destination file. For example, `mv file.txt Documents/file.txt` will move a file named "file.txt" to the "Documents" directory.

9. `module`:  load and unload software modules.

   ```bash
   module load gcc
   ```

   load the GNU Compiler Collection.

10. `sbatch`: This command is used to submit batch jobs to the HACC

11. `mpiexec`: This command is used to run parallel programs using the Message Passing Interface (MPI) library. MPI is a widely used programming model for distributed memory parallel computing. To use MPI on the HACC Cluster, you would typically load a module containing the MPI implementation you want to use (e.g. `module load openmpi`) and then run your program using the `mpiexec` command. For example, `mpiexec -n 4 myprogram` will run a parallel program named "myprogram" using 4 processes.

12. `htop`: This command is used to monitor system processes and resource usage. It provides a real-time view of system performance, including CPU usage, memory usage, and network activity. To use `htop`, simply type `htop` at the command prompt.

13. `scp`: This command is used to copy files between the HACC Cluster and your local machine. To copy a file from the HACC Cluster to your local machine, you would use the following command on your local machine:

    ```php
    scp <username>@hacc.rit.albany.edu:<file> <destination>
    ```

14. `sftp`: This command is used to securely transfer files between the HACC Cluster and your local machine using the Secure File Transfer Protocol (SFTP). To use `sftp`, type `sftp <username>@hacc.rit.albany.edu` at the command prompt. You will then be prompted to enter your HACC Cluster password. Once logged in, you can use `sftp` commands to navigate through the file system and transfer files between the HACC Cluster and your local machine.]

##### Running Compute Jobs:

HACC cluster uses the the Slurm job scheduler to manage resources. Here is a [command cheat sheet](https://slurm.schedmd.com/pdfs/summary.pdf) for using slurm. Or you may refer to their [quick start user guide.](https://slurm.schedmd.com/archive/slurm-17.11.2/quickstart.html)

In order to run an accelerated task, i.e. use an FPGA or GPU, you must submit a job request to the scheduler, by requesting how much time you want, what type of resources you require, and how many nodes you need.

You will use the linux command line to submit jobs to the Slurm scheduler, specify job parameters such as the number of nodes and cores required, monitor job progress, and retrieve output files once the job has completed.

For example, to submit a job to the Slurm scheduler, you might use the `sbatch` command followed by the name of a script containing the commands you want to run. The script might contain commands such as those I provided earlier, to navigate the file system, copy files, and execute programs.

So, in summary, the Slurm job scheduler manages the allocation of resources on the HACC Cluster, but you will still need to use the Linux command line to interact with the cluster and run your jobs.

##### Resource management:

HACC resources are virtualized, such as that, a single physical node has multiple VMs running on it, with a single FPGA/GPU allocated per VM. When your job is scheduled, an entire VM is exclusively allocated to your job. Once your job completes, the VM is released and another job maybe scheduled on it. For a list of all available nodes (VM and non-VM) nodes, please check the [cluster status page.](https://xilinx-center.csl.illinois.edu/xacc-cluster/xacc-cluster-status/)

Resources on HACC are managed in multiple job queues or resource partitions. Once you log into the login nodes, or a development node, you may submit jobs to these queues. In order to see what job queues you have access to, you may run the command:

```bash
sinfo
```

The result should look something like this:

![img](https://xilinx-center.csl.illinois.edu/files/2020/12/image-1.png)

Note that each job queue has a unique set of resources. For example, if you need to run a job on an Alveo U280, you will need to request a job on a queue labeled *“u280-xxx”.* Each queue has different time limits as well. So a job on u280-run can run for up to 1 hour, while a job in u280-short can run for up to 3 hours.

##### Writing a job script and submitting jobs:

Please refer to the SLURM user guide on how to write a job script. To get you started, here is a basic job script. Before you run your job, keep in mind:

1. Your environmental variable will be copied from the submission node.
2. Always use FULL paths in your scripts
3. Make sure your script runs your code in the correct directory
4. Make sure to source the XRT libs inside your script

Here is a sample script to run the basic xrt validate utility:

```
### alveo_test.script ###
#!/bin/sh
source /opt/xilinx/xrt/setup.sh
xbutil validate
```

You can then submit the hob and run it on an Alveo U250 by submitting it to one of the Alveo queues. For example:

```
sbatch -p u250-run alveo_test.script
```

You may provide many more options to the sbatch command to determine the name of your job, the number of requested nodes, error management etc. For details, please refer to the SLURM documentation.

**NOTE: Jobs can be submitted from login nodes or the development node.** 

Once a job is complete, your output is saved to a run log named *slurm-<jobid>.out* in your home directory by default. You may change the name and location of this via the job script.

You can monitor the state of your submitted jobs via

```
squeue
```

It is possible to request an interactive job. This will log you directly into a compute node, without the need to write a job script. Once logged in, you can interact with the node and run as many commands as you’d like within the allotted time limits. Please refer to the SLURM documentation on how to run interactive jobs. As an example, to interact with the U280 nodes:

```
srun -p u280-run -n 1 --pty bash -i
```



#### How to use the cluster

XACC has been designed with HPC principles in mind. As such, you can use an HPC job scheduler to help manage resources. You will need to submit “jobs” to the scheduler in order to run your tasks. You can not directly execute your jobs on a compute node. You can perform 2 types of operations on the cluster:

1. **Development – Compiling, synthesizing, and generating bitstreams for FPGA accelerators.**
   HACC have one shared development node for these activities. This node is NOT governed by the job scheduler right now. Users are free to directly SSH into the node, and compile their jobs.
   As the number of users increase, HACC may need to put the development node behind the scheduler. In this case, you will need to submit Vitis compilations as jobs as well. Note that HACC do not support the GUI based flow.The recommended operation is that users will perform their initial design and project setup on their local desktop machines. One the project is ready to be built, users upload their projects to the cluster and only perform compilation, synthesis, P&R etc on the cluster. No development is expected to be done on this node.
2. **Compute – Running accelerated kernels on FPGAs/GPUs**
   Once the project is built, users should have an host executable and a xilinx XCLbin file (partial bitstream). Users may then submit a job to the scheduler and request time on an accelerated compute node.

#### Cluster can’t do

- HACC only support Alveo flows. We do not currently support non-alveo cards.

- Please only use Vitis/Pynq to build your projects

- You may develop your hardware in C/C++ or OpenCL or RTL

- You must work with the installed Alveo shells.

- HACC do not support custom FPGA images or custom shells.

- HACC do not support custom OS/Kernels

- You will not have root access

- No JTAG debugging available

  

#### Details of Cluster

The cluster is comprised of 6 compute nodes + 4 Support nodes.

- 1 x Head node – Many core, large memory development node.
- 2 x Compute nodes – Accelerated compute node
- 2 x Big compute nodes – Accelerated compute nodes with support for up to eight accelerators per node
- 1 x Big Data node – FPGA + high end GPU node for big data analytics research
- 4 x Login nodes

The nodes are interconnected on a 100 Gb/s ethernet rack switch with a 1 Gb/s uplink to communicate outside of the cluster. For storage, there is a shared 24TB home filesystem. Each compute node also includes high speed scratch space.

![img](https://xilinx-center.csl.illinois.edu/files/2021/07/uiuc-xacc-setup-1001x1024.png)

Overall compute infrastructure details:

![Screenshot 2023-04-11 at 15.13.55](/Users/elio/Library/Application%20Support/typora-user-images/Screenshot%202023-04-11%20at%2015.13.55.png)



#### HACC Cluster Software

All HACC compute machines use Ubuntu 18.04

Toolchain support:

- Xilinx Vitis
- Xilinx XRT v2.6.655 on compute nodes
- Python v3
- Open MPI
- OpenMP
- SLURM v17

1. Xilinx Vitis: Xilinx Vitis is a software platform used for developing accelerated applications on Xilinx FPGAs. On the HACC Cluster, users can use Xilinx Vitis to develop and optimize FPGA-based applications, which can then be run on the cluster's FPGA-enabled nodes. The Xilinx Vitis platform is pre-installed on the HACC Cluster, so users can simply load the necessary modules and start using the tools.

   Steps to use Xilinx Vitis on HACC Cluster:

   1. Connect to the HACC Cluster using SSH.

   2. Load the `cuda` module by running the following command:

      ```lua
      module load cuda
      ```

   3. Load the `vitis` module by running the following command:

      ```lua
      module load vitis
      ```

   4. Load the `xilinx` module by running the following command:

      ```lua
      module load xilinx
      ```

   5. Once you have loaded the necessary modules, you can start using Xilinx Vitis to develop and optimize FPGA-based applications. Here are the general steps for using Xilinx Vitis:

      1. Create a new project in Vitis using the Project Wizard. You can access the Project Wizard by selecting File > New Project in the Vitis IDE.
      2. In the Project Wizard, select the appropriate project type based on your application requirements. For example, you might select the "Application Project" option to create an FPGA-accelerated application.
      3. Specify the project settings, such as the project name and location, target platform, and hardware settings.
      4. Use the Vitis IDE to develop and optimize your application code. You can write your application code in languages such as C, C++, and OpenCL.
      5. Once you have completed your application development and optimization, you can generate the FPGA bitstream using the Vitis tools.
      6. Finally, you can deploy your application to the HACC Cluster using Xilinx XRT, which is pre-installed on the compute nodes. To deploy your application, you will need to transfer the necessary files to the cluster, and then use Xilinx XRT to manage the FPGA resources and run the application.

2. Xilinx XRT: Xilinx XRT is a runtime library used for deploying and managing Xilinx FPGAs. On the HACC Cluster, Xilinx XRT v2.6.655 is installed on the compute nodes, allowing users to deploy FPGA-accelerated applications and manage the FPGA resources available on the cluster.
