//This document is mainly used to document mental notes and unfinished thoughts on networking before being ready to go in .md document
//Used as a guide to prepare IEX DEEP data on the HACC Cluster and how to transmit it

Setup:
    -Prepare IEX DEEP data on local machine by downloading from https://iextrading.com/trading/market-data/
    -If the file is not already in .pcap.gz format zip the file with 'gzip -k <filename>'
    -Move the file to their machine by using SCP through terminal or by uploading through their browser  http://xacc-web-000-1.csl.illinois.edu/ (it is much faster to send the zipped file and then unzip on their machine)
    -Create a directory to hold your sample data
    -In the top right corner of the browser, click upload and find your file
    -It may get stuck at 100%, just let it sit for a few minutes
    -Unzip the .pcap.gz file to a .pcap file by using 'gzip -d <filename>'

Analyzing Captured Data:
Wireshark:
    -Offload the captured data .pcap file to your local computer (zip before offloading and unzip on local device)
    -In Wireshark: Edit -> Preferences -> Under protocols click 'Display hidden protocol items' and 'Look for incomplete dissectors'
    -Under the list of all the protocols look for IEX.EQUITIES.DEEP.IEXTP.V1.0.LUA and click 'Show Message Data'
    -Examine your pcap data
tcpdump:
    -On HACC cluster terminal, run 'tcpdump <filename>'







Transmitting:
//Lijiajin TODO





Receiving:
//Jason TODO
    -To set up network recieving on a computer node first ssh into a free login node as described in the user guide

    -Create a script for SLURM to network capture (more details in SLURM document)
    -View which interfaces are able to be captured on with tcpdump -D 
    -Set up a network capture with tcpdump -i <NIC name> -w <filename.pcap>
    






    - Issue encountered on 4/17: HACC Cluster is unable to grant us tcpdump use, currently looking into more options


    


Extra Notes:
    -Look into --immediate-mode and --packet-buffered in tcpdump for potential algos
    -Look into --time-stamp-precision in tcpdump for capturing down to nanosecond Transmitting
    
