# Slurm Command

#### Job Submission

salloc - Obtain a job allocation.
sbatch - Submit a batch script for later execution.
srun - Obtain a 1ob allocation (as needed) and execute an application

| --array=<indexes><br />(eg.”–array = 1-10:)  | Job array specification<br />(batch command only)            |
| :------------------------------------------: | :----------------------------------------------------------- |
|               --account=<name>               | Account to be charged for resources<br />used.               |
| --begin=<time><br/>(e.g. "--begin=18:00:00") | Initiate job after specified time                            |
|              --clusters=<name>               | Clusters(s) to run the job (sbatch command only)             |
|            –constraint=<feature>             | Required node features                                       |
|            -cpu-per-task=<count>             | Number of CPUs required per task.                            |
|          –dependency=<state:jobid>           | Defer job until specified jobs reach specified state.        |
|              –error=<filename>               | File in which to store job error messages.                   |
|               -exclude=<names>               | Specific host names to exclude from job allocation.          |
|              -exclusive[=user]               | Allocated nodes can not be shared with other jobs/users.     |
|            -export=<name =value>             | Export identified environment variables.                     |
|             -gres=<name:count)>              | Generic resources required per node.                         |
|                – input=<name>                | File from which to read job input data.                      |
|               –﻿job-name=<name>               | Job name.                                                    |
|                   - label                    | Prepend task ID to output(srun command only).                |
|           -licenses=<name:count >            | License resources required for entire job                    |
|                  -mem=<MB>                   | Memory required per node.                                    |
|              -mem-per-cpu=<MB>               | Memory required per allocated CPU.                           |
|           -﻿N<minnodes[-maxnodes]>            | Node count required for the job.                             |
|                  -n<count>                   | Number of tasks to be launched.                              |
|              -nodelist=<names>               | Specific host names to include in job allocation.            |
|                -output=<name>                | File in which to store job output.                           |
|             – ﻿partition=<names>              | Partition/queue in which to run the job.                     |
|                 -gos=<name>                  | Quality Of Service.                                          |
|          -signal= B: <num>[(@time]           | Signal job when approaching time limit.                      |
|                 -time=<time>                 | Wall clock time limit.                                       |
|            wrap=<command string>             | Wrap specified command in a simple "sh" shell. (sbatch command only) |
|                                              |                                                              |

#### Accounting

sacct - Display accounting data.

| –allusers           | Displays all users jobs.                                     |
| ------------------- | ------------------------------------------------------------ |
| -accounts=<name>    | Displays jobs with specified accounts.                       |
| -endtime=<time>     | End of reporting period.                                     |
| -format=<spec>      | Format output.                                               |
| -name=<jobname>     | Display jobs that have any of these name(s).                 |
| -partition=<names>  | Comma separated list of partitions to select jobs and job steps from. |
| -state=<state list> | Display jobs with specified states.                          |
| starttime=<time>    | Start of reporting period.                                   |
|                     |                                                              |

sacetmgr - View and modify account information

Options:

| \- immediate | Commit changes immediately. |
| ------------ | --------------------------- |
| --parseable  | Output delimited by ‘\|’    |

Commands:

| add <ENTITY> <SPECS> create <ENTITY> <SPECS> | Add an entity. Identical to the create command. |
| -------------------------------------------- | ----------------------------------------------- |
| delete <ENTITY> where <SPECS>                | Delete the specified entities.                  |
| list <ENTITY> [<SPECS>]                      | Display information about the specific entity.  |
| modify <ENTITY> where <SPECS> set <SPECS>    | Modify an entity.                               |

Entities:

| account | Account associated with job.              |
| ------- | ----------------------------------------- |
| cluster | Cluster:Name parameter in the sturm.conf. |
| qos     | Quality of Service.                       |
| user    | User name in system.                      |

#### Job Management

sbcast - Transfer file to a job's compute nodes.

sbcast [options] SOURCE DESTINATION

| --force    | Replace previously existing file.                            |
| ---------- | ------------------------------------------------------------ |
| --preserve | Preserve modification times, access times, and access permissions. |

scancel - Signal jobs, job arrays, and/or job steps.

| \- account=<name>   | Operate only on jobs charging the specified account.         |
| ------------------- | ------------------------------------------------------------ |
| -name=<name>        | Operate only on jobs with specified name                     |
| -partition=<names>  | Operate only on jobs in the specified partition/queue.       |
| -qos=<name>         | Operate only on jobs using the specified quality of service. |
| -reservation=<name> | Operate only on jobs using the specified reservation.        |
| --state=<names>     | Operate only on jobs in the specified state.                 |
| --user=<name>       | Operate only on jobs from the specified user.                |
| --nodelist=<names>  | Operate only on jobs using the specified compute nodes.      |
|                     |                                                              |

Squeue- View information about jobs.

| --account=<name>                       | View only jobs with specified accounts.                      |
| -------------------------------------- | ------------------------------------------------------------ |
| --clusters=<name>                      | View jobs on specified clusters.                             |
| --format=<spec> (e.g."_-format=%i %j") | Output format to display. Specify fields, size, order, etc.  |
| --jobs<job_id list>                    | Comma separated list of job IDs to display.                  |
| --name=<name>                          | View only jobs with specified names.                         |
| -partition=<names>                     | View only jobs in specified partitions.                      |
| --priority                             | Sort jobs by priority.                                       |
| --qos=<name>                           | View only jobs with specified Qualities Of Service.          |
| --start                                | Report the expected start time and resources to be allocated for pending jobs in order of increasing start time. |
| --state=<names>                        | View only jobs with specified states.                        |
| --Users=<names>                        | View only jobs for specified users.                          |
|                                        |                                                              |

sinfo - View information about nodes and partitions.

| --all              | Display information about all partitions.                    |
| ------------------ | ------------------------------------------------------------ |
| --dead             | If set, only report state information for non responding (dead) nodes. |
| -format=<spec>     | Output format to display.                                    |
| -iterate=<seconds> | Print the state at specified interval.                       |
| - -long            | Print more detailed information.                             |
| -﻿- Node            | Print information in a node-oriented format.                 |
| -partition=<names> | View only specified partitions.                              |
| -reservation       | Display information about advanced reservations.             |
| -R                 | Display reasons nodes are in the down, drained, fail or failing state. |
| --state=<names>    | View only nodes specified states.                            |



scontrol - Used view and modify configuration and state.

Also see the sview graphical user interface version.

| –details  | Make show command print more details. |
| --------- | ------------------------------------- |
| -oneliner | Print information on one line.        |

Commands:

| create SPECIFICATION | Create a new partition or                                    |
| -------------------- | ------------------------------------------------------------ |
| delete SPECIFICATION | Delete the entry with the specified SPECIFICATION            |
| reconfigure          | All Slurm daemons will re-read the configuration file.       |
| requeue JOB_LIST     | Requeue a running, suspended or completed batch job.         |
| show ENTITY ID       | Display the state of the specified entity with the specified identification |
| update SPECIFICATION | Update job, step, node, partition, or reservation configuration per the supplied specification. |

#### Environment Variables

| SLURM_ARRAY_JOB_ID    | Set to the job ID if part of a job array   |
| --------------------- | ------------------------------------------ |
| SLURM_ARRAY_TASK_ID   | Set to the task ID if part of a job array. |
| SLURM_CLUSTER_NAME    | Name of the cluster executing the job.     |
| SLURM_CPUS_PER_TASK   | Number of CPUs requested per task          |
| SLURM_JOB_ACCOUNT     | Account name.                              |
| SLURM_JOB_ID          | Job ID.                                    |
| SLURM_JOB_NAME        | Job Name.                                  |
| SLUM_JOB_NODELIST     | Names of nodes allocated to job.           |
| SLUM_JOB_NUM_NODES    | Number of nodes allocated to job.          |
| SLURM_JOB_PARTITION   | Partition/queue running the job.           |
| SLURM_JOB_UID         | User ID of the job's owner.                |
| SLURM_JOB_USER        | User name of the job's owner.              |
| SLURM_RESTART_COUNT   | Number of times job has restarted.         |
| SLURM_PROCID          | Task ID (MPI rank).                        |
| SLURM_STEPvID         | Job step ID.                               |
| SLURM_STEP_NUM_ TASKS | Task count (number of MPI ranks).          |
|                       |                                            |

#### Daemons

| slurmctld | Executes on cluster's "head" node to manage workload.        |
| --------- | ------------------------------------------------------------ |
| slurmd    | Executes on each compute node to locally manage resources.   |
| slurmdbd  | Manages database of resources limits, licenses, and archives accounting records. |

