# HACC Cluster Transmission Side 



#### Running Development/Compilation Jobs:

Users can perform initial project setup and development on their local development nodes. Once the project is ready to be built, users can upload their projects to the cluster via the web-interface or SCP (SSH secure file copy).

Users can then run their compilations on the development server. You can access the development server via SSH from a login node.

####  Running Compute Jobs:

HACC cluster uses the the Slurm job scheduler to manage resources. Here is a [command cheat sheet](https://slurm.schedmd.com/pdfs/summary.pdf) for using slurm. Or you may refer to their [quick start user guide.](https://slurm.schedmd.com/archive/slurm-17.11.2/quickstart.html)

In order to run an accelerated task, i.e. use an FPGA or GPU, you must submit a job request to the scheduler, by requesting how much time you want, what type of resources you require, and how many nodes you need.

#### Resource management:

HACC resources are virtualized, such as that, a single physical node has multiple VMs running on it, with a single FPGA/GPU allocated per VM. When your job is scheduled, an entire VM is exclusively allocated to your job. Once your job completes, the VM is released and another job maybe scheduled on it. For a list of all available nodes (VM and non-VM) nodes, please check the [cluster status page.](https://xilinx-center.csl.illinois.edu/xacc-cluster/xacc-cluster-status/)

Resources on HACC are managed in multiple job queues or resource partitions. Once you log into the login nodes, or a development node, you may submit jobs to these queues. In order to see what job queues you have access to, you may run the command:

```
sinfo
```

The result should look something like this:

![img](https://xilinx-center.csl.illinois.edu/files/2020/12/image-1.png)

Note that each job queue has a unique set of resources. For example, if you need to run a job on an Alveo U280, you will need to request a job on a queue labeled *“u280-xxx”.* Each queue has different time limits as well. So a job on u280-run can run for up to 1 hour, while a job in u280-short can run for up to 3 hours.

#### Steps to submit a job with SLURM commands on the HACC Cluster

1. Write a job script that contains the necessary SLURM commands and job parameters. This should include the full path to your executable code, and the XRT libraries should be sourced inside the script.

Here is an example job script to submit a job to run on the Alveo U250:

```
#!/bin/sh
#SBATCH --job-name=myjob
#SBATCH --ntasks=1
#SBATCH --partition=compute-run
#SBATCH --time=00:30:00
#SBATCH --output=myjob.out
#SBATCH --error=myjob.err

# Load XRT
source /opt/xilinx/xrt/setup.sh 2>/dev/null || true

scp /Users/elio/Desktop/IEX\ data/data_feeds_20230313_20230313_IEXTP1_DEEP1.0.pcap llin17@xacc-lg-000-1.csl.illinois.edu:/mnt/shared/home/llin17

# Output "It is a test"
echo "It is a test"
```
Or

```
#!/bin/sh
#SBATCH --job-name=myjob
#SBATCH --ntasks=1
#SBATCH --partition=u250-run
#SBATCH --time=00:30:00
#SBATCH --output=myjob.out
#SBATCH --error=myjob.err

# Load XRT
source /opt/xilinx/xrt/setup.sh

# Run your executable code
#/opt/path/to/my/executable
```

This script specifies the job name, number of tasks, partition, time limit, output and error file names, and the command to run your executable code.

2. Submit the job using the `sbatch` command and specifying the job script file:

```
sbatch myjobscript.sh
```

This will submit the job to the specified queue for execution.

3. Monitor the job status using the `squeue` command:

```
squeue -u yourusername
```

This will show you the status of all jobs submitted by your user account.

4. Once the job is complete, your output will be saved to a run log named `slurm-<jobid>.out` in your home directory by default.

Note that you may provide many more options to the `sbatch` command to determine the name of your job, the number of requested nodes, error management, etc. For more details on SLURM commands, you can refer to the SLURM user guide and the HACC Cluster documentation.

Additionally, if you need to run an interactive job, you can use the `srun` command. For example, to interact with the U280 nodes, you can use the following command:

```
srun -p u280-run -n 1 --pty bash -i
```

This will log you directly into a compute node, without the need to write a job script. Once logged in, you can interact with the node and run as many commands as you’d like within the allotted time limits.

For looping the IEX data
```
#!/bin/bash
#SBATCH --job-name=loopback

#SBATCH --time=00:30:30
#SBATCH --partition=u250-short
#SBATCH --ntasks-per-node=1
#SBATCH --output=loopback.out
#SBATCH --error=loopback.err

# Load XRT
. /opt/xilinx/xrt/setup.sh

# Define the input file path
input_file=/path/to/iex_data

# Loop over the first 100 lines of the input file
for ((i=1; i<=100; i++))
do
    # Read the ith line of the input file
    line=$(sed "${i}q;d" $input_file)

    # Transfer the line to the NIC using netcat
    echo $line | nc -w 1 $NIC_IP $NIC_PORT

    # Echo a message to show that the line has been transferred
    echo "Line $i transferred to $NIC_IP:$NIC_PORT"
done

# Output "It is a test"
echo "It is a test"
```
without test until now
