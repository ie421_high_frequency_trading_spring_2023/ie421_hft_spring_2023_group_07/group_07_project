# Research on Vitis and Hardware development

#### General idea

Vitis is a software platform created by Xilinx for developing applications that can run on Xilinx's FPGA (Field Programmable Gate Array) devices. The platform provides a comprehensive set of tools, libraries, and APIs that allow developers to design and optimize custom hardware accelerators and integrate them into larger software systems.

The hardware development process for designing FPGA-based systems typically involves the following steps:

1. System Specification: The first step in hardware development is to define the requirements and specifications for the system. This includes determining the functionality of the system, the performance requirements, and any design constraints or limitations.
2. Design: Once the specifications have been defined, the next step is to design the hardware. This involves creating a schematic or a high-level design description that defines the components of the system and how they are interconnected.
3. Verification: After the design is complete, it is important to verify that it meets the specifications. This can be done through simulation or by prototyping the design on an FPGA development board.
4. Synthesis: Once the design has been verified, it needs to be synthesized into a form that can be programmed onto an FPGA. This involves converting the high-level design into a netlist, which is a list of interconnected logic gates.
5. Implementation: The next step is to implement the design on an FPGA. This involves using software tools to map the netlist onto the FPGA and configure the device to perform the desired functionality.
6. Testing: After the implementation is complete, the system needs to be tested to ensure that it meets the specifications and performs as expected.

Vitis simplifies the hardware development process by providing a comprehensive set of tools and libraries that can be used to design and optimize custom hardware accelerators. The platform includes a high-level programming model called OpenCL, which allows developers to write software code that can be compiled to run on FPGA-based hardware. This makes it easier for software developers to leverage the power of FPGA-based accelerators in their applications without needing to have a deep understanding of hardware design.

#### Possible networking stack

1. Application layer: This layer includes the protocols that are used by applications to exchange data. Common application layer protocols include HTTP, FTP, SMTP, and DNS.

2. Transport layer: This layer provides end-to-end communication between hosts. It includes protocols like Transmission Control Protocol (TCP) and User Datagram Protocol (UDP).

3. Network layer: This layer is responsible for routing data between different networks. The Internet Protocol (IP) is an example of a network layer protocol.

4. Data link layer: This layer provides reliable communication between nodes on the same network. Examples of data link layer protocols include Ethernet and Wi-Fi.

5. Physical layer: This layer defines the physical medium used for communication, such as copper wires, optical fibers, or radio waves.


#### Common Infrastructure
This section provides a brief overview of the common infrastructure needed for the examples to work. The examples rely on the same underlying infrastructure, which is cmac and network_layer kernels.



NOTE: the reference clock frequency can change depending on the Alveo card.

CMAC kernel
The cmac_kernel contains an UltraScale+ Integrated 100G Ethernet Subsystem. This kernel is configured according to the INTERFACE, DEVICE, and PADDING_MODE arguments passed to make. It exposes two 512-bit AXI4-Stream interfaces (S_AXIS and M_AXIS) to the user logic, which run at the same frequency as the kernel, internally it has CDC (clock domain crossing) logic to convert from kernel clock to the 100G Ethernet Subsystem clock. It also provides and AXI4-Lite interface to check cmac statistics.

For more information check out Ethernet/README.md.

#### Network Layer kernel
The network layer kernel is a collection of HLS modules to provide basic network functionality. It exposes two 512-bit (with 16-bit TDEST) AXI4-Stream to the application, S_AXIS_sk2nl and M_AXIS_nl2sk.

The ARP table is readable from the host side, and the UDP table is configurable from the host as well. Helper functions to read and configure the tables are available in the class NetworkLayer Notebooks/vnx_utils.py.

The application communicates with the UDP module using S_AXIS_sk2nl and M_AXIS_nl2sk AXI4-Stream interfaces with the following structure:

struct my_axis_udp {
  ap_uint<512>    data;
  ap_uint< 64>    keep;
  ap_uint< 16>    dest;
  ap_uint<  1>    last;
}
In the Rx path, the network layer provides additional metadata in the user signal. To find out more about this kernel check out NetLayers/README.md

#### Examples
Basic
The following figure depicts the different kernels and their interconnection in the Vitis project for the basic example.

![image-1.png](./image-1.png)

NOTE: the reference clock frequency can change depending on the Alveo card.

cmac and network layer kernels are explained in the section above. In this example the application is split into two kernels, memory mapped to stream (mm2s) and stream to memory mapped (s2mm).

mm2s: pulls data from global memory and converts it to a 512-bit stream. It chunks the data into 1408-Byte packets, meaning that last is asserted. It also asserts last when there is no more data to send. The dest is set according to the argument with the same name.

s2mm: gets payload from the UDP module and push the payload to global memory. dest and last are ignored in this module.

The current limitation of this application is that the size of the data must be multiple of 64-Byte to work properly.

Check out vnx-basic notebook to see how to run this example using pynq.

Benchmark
The following figure depicts the benchmark example design, which contains four benchmark kernels. Each benchmark kernel has two modules, traffic generator and collector.



NOTE: the reference clock frequency can change depending on the Alveo card.

Find out more information about the benchmark kernel in Benchmark_kernel/README.md

The following notebooks demonstrate how to use the benchmark example design to measure throughput and latency either point to point or with a switch connection between two Alveo cards.

vnx-benchmark-throughput
vnx-benchmark-throughput-switch
vnx-benchmark-rtt
vnx-benchmark-rtt-switch
Support
Tools
In order to implement this design you need Vitis 2021.1 or newer and associated XRT. Older version of the tools are discouraged and will not work.

Vitis	XRT	pynq	Notes
```
2021.1	2.11.634	>=2.7	Apply Y2K22 patch, consider using a newer Vitis version
2021.2	2.12.427	>=3.0.1	Apply Y2K22 patch, consider using a newer Vitis version
2022.1	2.13.466	>=3.0.1	
2022.2	2.14.354	>=3.0.1	
```
To install pynq and JupyterLab follow the steps here.

```
Alveo Cards
Alveo	Development Target Platform(s)	Notes
U50	xilinx_u50_gen3x16_xdma_201920_3	Supported only until 2021.2
U50	xilinx_u50_gen3x16_xdma_5_202210_1	
U55C	xilinx_u55c_gen3x16_xdma_2_202110_1	
U55C	xilinx_u55c_gen3x16_xdma_3_202210_1	
U200	xilinx_u200_gen3x16_xdma_1_202110_1	
U250	xilinx_u250_gen3x16_xdma_3_1_202020_1	DFX two-stage platform
U250	xilinx_u250_gen3x16_xdma_4_1_202210_1	DFX two-stage platform
U280	xilinx_u280_xdma_201920_3	Supported only until 2021.2
```

#### Requirements
In order to generate this design you will need a valid UltraScale+ Integrated 100G Ethernet Subsystem license set up in Vivado.

You also need at least 32GB of RAM available for the implementation. Check Vivado memory requirements here.

#### Generate XCLBIN
To implement any of the examples run:

make all DEVICE=<full platform path> INTERFACE=<interface number> DESIGN=<design name>
DEVICE Alveo development target platform, see supported platforms
INTERFACE defines which physical interface is going to be use. 0, 1 or 3 are supported. When INTERFACE=3 the design will be replicated for each interface. Note that Alveo U50 only has one interface available (INTERFACE=0)
DESIGN only support the following strings basic and benchmark. If you use something different, an error will be reported
The basic configuration file is pulled from config_files folder and complete with userPostSysLinkOverlayTcl parameter before calling v++.
Ethernet/post_sys_link.tcl is automatically called from v++ after system link. It is used to connect the GT capable pins to the cmac kernel(s)
The XCLBIN will be generated in the folder <DESIGN>.intf_<INTERFACE>.<(short)DEVICE>
```
Repository structure
├── Basic_kernels
│   └── src
├── Benchmark_kernel
│   └── src
├── config_files
├── Ethernet
│   └── cmac
├── img
├── NetLayers
│   ├── 100G-fpga-network-stack-core
│   └── src
├── Notebooks
└── xrt_host_api
    ├── examples
    ├── include

```
Basic_kernels: this folder contains the code for the basic application (mm2s and s2mm)
Benchmark_kernel: this folder contains the benchmark application, which includes the collector, traffic generator and switch kernels
config_files: this folder contains the different configuration files for the different examples and flavors
Ethernet: this folder contains the logic to generate the cmac kernel for the different Alveo cards and interfaces
img: this folder contains images
NetLayers: this folder contains the logic to generate the network_layer kernel, using the submodule 100G-fpga-network-stack-core
Notebooks: this folder contains companion Jupyter notebooks to show how to run the different examples
xrt_host_api: C++ driver and examples
