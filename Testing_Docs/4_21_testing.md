# 4/21/23 Testing Documentation
#
## Tasks performed today
#
## Jason testing validate on FPGA
1. ssh into login node
2. created validate script:
```css
#!/bin/sh
source /opt/xilinx/xrt/setup.sh
xbutil validate
```
3. Job was sent to U250 - and failed as expected due to FPGA networking issues. Output from .out file:
```css
/var/spool/slurmd/job02947/slurm_script: 2: /var/spool/slurmd/job02947/slurm_script: source: not found
/var/spool/slurmd/job02947/slurm_script: 3: /var/spool/slurmd/job02947/slurm_script: xbutil: not found
```
Results: Job was able to be successfully sent through SLURM, but ran into issues with the validate software, which we think is due to the FPGAs being down.


## Jason testing receive on compute-node
1. ssh into xacc-head-000-5
2. Created calling script:
```css
#!/bin/sh
#SBATCH --job-name=rx_test
#SBATCH --ntasks=1
#SBATCH --partition=compute-run
#SBATCH --time=00:00:30
#SBATCH --output=rx_test.out
#SBATCH --output=rx_test.err

source /mnt/shared/home/jasonar2/scripts/rx_script_mac.py
```
3. Called rx_script_mac to collect all incoming packets on NIC: this script is defined in HACC_Networking_Receiving_Guide.md under Steps to network capture on NIC with a MAC address with python scripting
4. Job was sent to compute-node, however compute-node is in drain state and is being held in the queue
Results: compute-node is the only partition that we can ssh into and get access to a network card. Every other partition is either a FPGA or a GPU, which would not allow us to capture our data for this first part of the project. Might have to reach out to Scott to check access or move on to tickerplant dev.

## Elio testing sending on compute-node
1. ssh into login node
2. created script:
```css
#!/bin/sh
#SBATCH --job-name=myjob
#SBATCH --ntasks=1
#SBATCH --partition=compute-run
#SBATCH --time=00:30:00
#SBATCH --output=myjob.out
#SBATCH --error=myjob.err

# Load XRT
source /opt/xilinx/xrt/setup.sh 2>/dev/null || true

# Output "It is a test"
echo "It is a test"
```
3. Job was successfully submited to U250 and compute node. Output from myjob.out file:
```css
It is a test
```
Results: Job was able to be successfully submit through SLURM, and we could get correct output. However, the status of our job is keep pending and we think is due to the FPGAs being down. As the above explaination showed, compute-node is in drain state and is being held in the queue. I will start to prepare small size data for further test. 
