# __Group 7 Final Report__

## Project Goals:
1. Bring up a board to run hardware based trading algorithms/analysis

2. Learn to develop on industry level software for FPGAs

3. Have a platform we can comfortably develop on, and allow for future groups to easily implement new algorithms/ analysis



## VM Bringup

The heart of our project relies on the development virtual machine. This provides a standardized platform for all team members to develop on, and not have to worry about random dependencies that may or may not exist on his/her system. Many of the IPs to develop on the Alveo boards require an Ubuntu system, so the VM was setup on Ubuntu 18.04.

The steps below highlight some of the important parts when setting up the development VM. Detailed instructions for getting setup can be found on our repo [here]( https://gitlab-beta.engr.illinois.edu/ie421_high_frequency_trading_spring_2023/ie421_hft_spring_2023_group_07/group_07_project/-/blob/main/Quickstart_Guide.md?ref_type=heads)


Multiple facets of the VM must be setup sequentially. 

1. VirtualBox needs to be setup and installed and an Ubuntu 18.04 iso needs to be downloaded.

2. The VM's virtual hard disk must have more than 300 gb of space to make room for all of the software.

3. Grant your user sudo privileges and download & install the Xilinx Unified Installer. This part takes hours.

4. Download & install the proper XRT, Deployment Target Platform, Development Target Platform and board files for the U250 and create a new directory to store the downloads.

5. Run Vitis/Vivado by sourcing the settings and openning an example project on the U250 board.

Once the VM has been confirmed to work in the professor's new HFT lab, the setup will be moved to Vagrant to automate the tedious setup process.

## HACC Cluster
The HACC Cluster is a high-performance computing (HPC) system that uses the Linux operating system. It is designed to support high-performance computing (HPC), machine learning (ML), investigating programming models, compilers, schedulers, and systems. It is scalable and expandable, and the most important thing is that it encompasses modern FPGAs. 

Please refer to the cluster documentation here: 
https://xilinx-center.csl.illinois.edu/xacc-cluster/xacc-user-guide/ 

#### How to use the cluster

XACC has been designed with HPC principles in mind. As such, you can use an HPC job scheduler to help manage resources. You will need to submit “jobs” to the scheduler in order to run your tasks. You can not directly execute your jobs on a compute node. You can perform 2 types of operations on the cluster:

1. **Development – Compiling, synthesizing, and generating bitstreams for FPGA accelerators.**
   HACC have one shared development node for these activities. This node is NOT governed by the job scheduler right now. Users are free to directly SSH into the node, and compile their jobs.
   As the number of users increase, HACC may need to put the development node behind the scheduler. In this case, you will need to submit Vitis compilations as jobs as well. Note that HACC do not support the GUI based flow.The recommended operation is that users will perform their initial design and project setup on their local desktop machines. One the project is ready to be built, users upload their projects to the cluster and only perform compilation, synthesis, P&R etc on the cluster. No development is expected to be done on this node.
2. **Compute – Running accelerated kernels on FPGAs/GPUs**
   Once the project is built, users should have an host executable and a xilinx XCLbin file (partial bitstream). Users may then submit a job to the scheduler and request time on an accelerated compute node.
 
#### Details of Cluster
The UIUC-XACC cluster has been designed for Xilinx Alveo cards as the primary accelerators. Thus, the programming and execution model has been setup to enable easy and reliable access to the FPGA accelerators.

There are several different cluster machines with varied resources. Note that only some machines are open to external users. You can check the specifications and accessibility of machines [Here](https://xilinx-center.csl.illinois.edu/xacc-cluster/). For security, isolation, and ease of management we have virtualized the cluster machines. For a list of all available nodes (VM and non-VM) nodes, please check the [cluster status page](https://xilinx-center.csl.illinois.edu/xacc-cluster/hacc-cluster-status/).

The cluster status page will be periodically updated to reflect changes in node status, availability, and resources.

The cluster is comprised of 6 compute nodes + 4 Support nodes.

- 1 x Head node – Many core, large memory development node.
- 2 x Compute nodes – Accelerated compute node
- 2 x Big compute nodes – Accelerated compute nodes with support for up to eight accelerators per node
- 1 x Big Data node – FPGA + high end GPU node for big data analytics research
- 4 x Login nodes

The nodes are interconnected on a 100 Gb/s ethernet rack switch with a 1 Gb/s uplink to communicate outside of the cluster. For storage, there is a shared 24TB home filesystem. Each compute node also includes high speed scratch space.

![img](https://xilinx-center.csl.illinois.edu/files/2021/07/uiuc-xacc-setup-1001x1024.png)

Overall compute infrastructure details:
Active Nodes on HACC Cluster and Associated Compute Resources

![img](images/Node_Status.png)

HACC_FPGA Status:
List of current publicly available FPGAs denoting the the FPGA name, the installed shell, and the VM it is currently passed through to. Note that the VM name shows which physical machine the FPGA is connected to, which can be used to determine what type of access is available on the FPGA i.e. if it is connected to the 100G switch.

![img](/images/FPGA_Status.png)


#### Cluster can’t do

- HACC only support Alveo flows. We do not currently support non-alveo cards.

- Please only use Vitis/Pynq to build your projects

- You may develop your hardware in C/C++ or OpenCL or RTL

- You must work with the installed Alveo shells.

- HACC do not support custom FPGA images or custom shells.

- HACC do not support custom OS/Kernels

- You will not have root access

- No JTAG debugging available

##### Access HACC Cluster
To access the HACC Cluster, you will need an account and access credentials provided by your institution's HPC administrator. To request access to XACC, please contact the the xilinx-center. Upon completion of the above checklist, the system admin will provide you with an XACC account. Once you have access to the HACC Cluster, you can log in via the command line interface (CLI) using a Secure Shell (SSH) client or login the web-client. To do this, open a terminal on your local machine and enter the following command:

Direct SSH:

```css
xacc-lg-000-1.csl.illinois.edu
xacc-lg-001-1.csl.illinois.edu
xacc-lg-002-1.csl.illinois.edu
```

If you are a UIUC user, you can use the follwing hostname

```css
ssh <username>@xacc-head-000-5.csl.illinois.edu
```
Replace <username> with your HACC Cluster username.

Users outside UIUC must log into the UIUC VPN via their provided netID and password, and then ssh into one of the login nodes. Details on UIUC VPN access are available [here](https://help.uillinois.edu/TDClient/42/UIUC/Requests/ServiceDet?ID=167).
The login nodes provide an entry point to the cluster. You can submit and monitor jobs from here. You may also access and edit files on these nodes. 

Users can then run their compilations on the development server. You can access the development server via SSH from a login node.

```css
xacc-head-000-5.csl.illinois.edu
```

Web Access:
All users may also access the cluster via the [Open OnDemand web client](http://xacc-web-000-1.csl.illinois.edu/). 

The web client will allow you to log into the cluster without needing to SSH or use a VPN. You can view, edit, upload, and download your files from the web client, and it also provides you with a web-based linux terminal to ssh into the login nodes and submit jobs.

Once you log into the web-client, you may use the terminal to ssh into a login node to begin to use the cluster.

##### Basic command line for HACC_Cluster
Once you have logged in, you will be in your home directory. To navigate through the HACC Cluster, you will need to use the command line interface. Here are some basic commands that you can use to navigate through the file system:

1. `module`:  load and unload software modules.

   ```bash
   module load gcc
   ```

   load the GNU Compiler Collection.

2. `sbatch`: This command is used to submit batch jobs to the HACC

3. `mpiexec`: This command is used to run parallel programs using the Message Passing Interface (MPI) library. MPI is a widely used programming model for distributed memory parallel computing. To use MPI on the HACC Cluster, you would typically load a module containing the MPI implementation you want to use (e.g. `module load openmpi`) and then run your program using the `mpiexec` command. For example, `mpiexec -n 4 myprogram` will run a parallel program named "myprogram" using 4 processes.

4. `htop`: This command is used to monitor system processes and resource usage. It provides a real-time view of system performance, including CPU usage, memory usage, and network activity. To use `htop`, simply type `htop` at the command prompt.

5. `scp`: This command is used to copy files between the HACC Cluster and your local machine. To copy a file from the HACC Cluster to your local machine, you would use the following command on your local machine:

    ```php
    scp <username>@hacc.rit.albany.edu:<file> <destination>
    ```

6. `sftp`: This command is used to securely transfer files between the HACC Cluster and your local machine using the Secure File Transfer Protocol (SFTP). To use `sftp`, type `sftp <username>@hacc.rit.albany.edu` at the command prompt. You will then be prompted to enter your HACC Cluster password. Once logged in, you can use `sftp` commands to navigate through the file system and transfer files between the HACC Cluster and your local machine.

##### HACC Networked FPGA Guide
FPGAs in Compute Nodes 2 and 3 have both QSFP cages directly connected to the 100Gb/s Top-of-Rack (TOR) switch. While some of those FPGAs are dedicated to particular users, other FPGAs are flexibly provided to all HACC users through SLURM queues. For example,  u280-networked-short, u280-networked-medium, and u280-networked-long are the SLURM queues to launch jobs on a VM with a network attached U280. Like other SLURM accessible FPGAs, these FPGAs are restricted to flows compatible with Xilinx provided shells.

Please note that when a session ends, the design will still be loaded on the FPGA. This means it will continue to generate network traffic until it is reprogrammed. A simple way to stop this is to run 
```php
xbutil validate
```
which will load a new design on the FPGA which doesn’t utilize the network interfaces.

## __Board Bringup__

### Alveo U280 
Features a Xilinx UltraScale+ VU9P FPGA. It has a massive logical capacity as well, featuring 1,079,000 LUTs and 32 GB of DDR4 memory with 460 GB/s memory bandwidth. Connectivity to this board can be done through QSFP transceivers supporting up to 100 GB ethernet, as well as PCI Express. 

This board was our original development board, however after learning some of the Xilinx Advanced Algorithmic Trading IP cores are not compatible on the U280, as well as the U250 being a more documented board, we decided to transition to the U250.

### Alveo U250 
This board has very similar specs to its U280 counterpart. The main differences lie in having more LUTs (1,341,000), while having no DRAM and more SRAM. Like mentioned above, this board has been used in example fintech projects and is compatible with the AAT IP catalog. The development VM has been configured to develop on this board.

### Alveo X3522
This board is located in the professors HFT lab and will be continued to be developed on through the fall semester. The X3 supports 1 million LUTs with similar DSFP cages and PCI Express. This board has less compatibility with existing IP cores, however we would be able to develop applications in Vivado instead of Vitis, as we have access directly to the FPGA. 


## __IPs__

### Advanced Algorithmic Trading (AAT)
Includes many prebuilt cores that can process CME market data as well including simple trading strategies. Must be used in Vitis and is fully written in HLS, (for software developers). Is only supported on the U50 and U250 and is royalty free. Request had to be granted from Xilinx support team to access these IPs.

This block diagram shows how the CME IP core works. Data is seen through UDP market messages, and based on software configuration, certain messages can be used to build the orderbook and pricing engine.

![Alt text](image.png)

Limitations:

- Only works with UDP
- Has only been tested by Xilinx with user-created PCAP files
- No timestamping

### 10G/25G Ethernet Subsystem

This IP core provides a 10 gb/s or a 25 gb/s Ethernet Media Access Controller

In order to gain access to this IP, Xilinx support had to be contacted and a node-locked license had to be generated for each VM.

To create the license, generate one from [Xilinx's page](https://www.xilinx.com/products/intellectual-property/cmac_usplus/cmac_usplus_order.html). On the Vivado License Manager (VLM) input your computer's hostname and host id. You can find these on your machine with ifconfig. Next, download the .lic file and import it into the VLM. You can now drag and drop it into Vivado/Vitis projects

### VNx

VNx is an open source IP that can allow up to 100 Gb/s networking to a module. VNx's main usecase is extracting UDP payloads at very high bandwidths. This will be used to extract the market data from Ethernet. The open source VNx repo contains all the source code, scripts to compile and link the kernels, and pynq to host the code.  

![Alt text](image-1.png)

As VNx is a wrapper around the 100G Ethernet Subsystem (an updgraded version of the subsystem listed above), a node-locked license must be generated of that Xilinx IP.

In order to set up Pynq to develop with VNx, first install anaconda onto the development VM.
```css
wget https://repo.anaconda.com/archive/Anaconda3-2022.05-Linux-x86_64.sh -O anaconda3.sh
bash anaconda3.sh
```
Install Jupyter Lab inside the conda environment
```css
conda install -c conda-forge jupyterlab
```
Finally use pip to get pynq
```css
pip install pynq
```

### XRT (Xilinx Runtime)

XRT is a software stack and runtime library for Xilinx FPGAs. It provides a standardized runtime environment by abstracting away much of the hardware. XRT also includes many device drivers that allow the host CPU to communicate with the actual FPGA. This makes it a necessity for our project. Our VM uses XRT version 2022 10.2.13.466_18.04.

## Group Expansion

Over the summer, 2 new members had joined the team part-time. Another 2 members joined the team at the beginning of the 2023 fall semester. A considerable amount of time was spent into getting them onboarded onto the project. As the project continues to grow, seperate teams with different leaders may be created in order to effectively manage workload.


## Vitis Development

As Vitis is a program primarily targeted towards software engineers a lot of our project was spent learning Vitis. Xilinx does a good job at providing lots of open source training modules and learning sessions. Our group spent some time completing many of the training modules on https://github.com/Xilinx/Vitis-Tutorials/tree/2022.2/Hardware_Acceleration/Feature_Tutorials

Some of the highlights from the training course:
- Lab 1: Getting started with RTL Kernels: This lab taught us how the cross compilation works and are able to generate .xclbin files that can be sent to HACC Cluster. Another highlight from this lab was packaging RTL modules as an IP and and develop in Vivado.

- Lab 2: Mixing C++ and RTL Kernels: This lab taught us how to actually get started on building an application, a starting point for our actual board bringup. Also showed us some debug tools with emulation.
![Alt text](image-2.png)

- Lab 9: Using Ethernet on Alveo: This lab taught us how to handle inputs from a QSFP cage and how to read packets. This lab was also useful in showing how to link Vivado and Vitis projects together.

![Alt text](image-3.png)

Some other trainings outside of this repo were from the [Xilinx University Program Tutorial]( https://xilinx.github.io/xup_compute_acceleration/). This was mainly used to show how to get Vitis setup and how to use all the software and hardware debug tools.

Overall all group members were able to grasp the functionality of Vitis and were able to complete the trainings.

## Tx/Rx
#### Transmission Side 

##### Running Development/Compilation Jobs:

Users can perform initial project setup and development on their local development nodes. Once the project is ready to be built, users can upload their projects to the cluster via the web-interface or SCP (SSH secure file copy).

Users can then run their compilations on the development server. You can access the development server via SSH from a login node.

##### HACC Cluster Software:
ll HACC compute machines use Ubuntu 18.04

Toolchain support:

- Xilinx Vitis
- Xilinx XRT v2.6.655 on compute nodes
- Python v3
- Open MPI
- OpenMP
- SLURM v17

1. Xilinx Vitis: Xilinx Vitis is a software platform used for developing accelerated applications on Xilinx FPGAs. On the HACC Cluster, users can use Xilinx Vitis to develop and optimize FPGA-based applications, which can then be run on the cluster's FPGA-enabled nodes. The Xilinx Vitis platform is pre-installed on the HACC Cluster, so users can simply load the necessary modules and start using the tools.

   Steps to use Xilinx Vitis on HACC Cluster:

   1. Connect to the HACC Cluster using SSH.

   2. Load the `cuda` module by running the following command:

      ```lua
      module load cuda
      ```

   3. Load the `vitis` module by running the following command:

      ```lua
      module load vitis
      ```

   4. Once you have loaded the necessary modules, you can start using Xilinx Vitis to develop and optimize FPGA-based applications. Here are the general steps for using Xilinx Vitis:

      1. Create a new project in Vitis using the Project Wizard. You can access the Project Wizard by selecting File > New Project in the Vitis IDE.
      2. In the Project Wizard, select the appropriate project type based on your application requirements. For example, you might select the "Application Project" option to create an FPGA-accelerated application.
      3. Specify the project settings, such as the project name and location, target platform, and hardware settings.
      4. Use the Vitis IDE to develop and optimize your application code. You can write your application code in languages such as C, C++, and OpenCL.
      5. Once you have completed your application development and optimization, you can generate the FPGA bitstream using the Vitis tools.
      6. Finally, you can deploy your application to the HACC Cluster using Xilinx XRT, which is pre-installed on the compute nodes. To deploy your application, you will need to transfer the necessary files to the cluster, and then use Xilinx XRT to manage the FPGA resources and run the application.

Here is a template Vitis Project I have uploaded to HACC cluster which is 
![img](/images/template_vitis_project.png)
![img](/images/size_of_project.png)
##### Running Compute Jobs:

In order to run an accelerated task, i.e. use an FPGA or GPU, you must submit a job request to the scheduler, by requesting how much time you want, what type of resources you require, and how many nodes you need. You will use the linux command line to submit jobs to the Slurm scheduler, specify job parameters such as the number of nodes and cores required, monitor job progress, and retrieve output files once the job has completed. For example, to submit a job to the Slurm scheduler, you might use the `sbatch` command followed by the name of a script containing the commands you want to run. The script might contain commands such as those I provided earlier, to navigate the file system, copy files, and execute programs. So, in summary, the Slurm job scheduler manages the allocation of resources on the HACC Cluster, but you will still need to use the Linux command line to interact with the cluster and run your jobs.

Resource management:
HACC resources are virtualized, such as that, a single physical node has multiple VMs running on it, with a single FPGA/GPU allocated per VM. When your job is scheduled, an entire VM is exclusively allocated to your job. Once your job completes, the VM is released and another job maybe scheduled on it. For a list of all available nodes (VM and non-VM) nodes, please check the [cluster status page.](https://xilinx-center.csl.illinois.edu/xacc-cluster/xacc-cluster-status/)

Resources on HACC are managed in multiple job queues or resource partitions. Once you log into the login nodes, or a development node, you may submit jobs to these queues. In order to see what job queues you have access to, you may run the command:

```bash
sinfo
```

The result should look something like this:

![img](https://xilinx-center.csl.illinois.edu/files/2020/12/image-1.png)
Note that each job queue has a unique set of resources. For example, if you need to run a job on an Alveo U280, you will need to request a job on a queue labeled *“u280-xxx”.* Each queue has different time limits as well. So a job on u280-run can run for up to 1 hour, while a job in u280-short can run for up to 3 hours.

Writing a job script and submitting jobs:
Please refer to the SLURM user guide on how to write a job script. To get you started, here is a basic job script. Before you run your job, keep in mind:

1. Your environmental variable will be copied from the submission node.
2. Always use FULL paths in your scripts
3. Make sure your script runs your code in the correct directory
4. Make sure to source the XRT libs inside your script

Here is a sample script test_jobsubmission.sh to run the basic xrt validate utility:

```
### test_jobsubmission.sh ###
#!/bin/sh
#SBATCH --job-name=submission_test
#SBATCH --ntasks=1
#SBATCH --partition=compute-run
#SBATCH --time=00:30:00
#SBATCH --output=test_jobsubmission.out
#SBATCH --error=test_jobsubmission.err

# Load XRT
source /opt/xilinx/xrt/setup.sh 2>/dev/null || true

scp /Users/elio/Desktop/IEX\ data/data_feeds_20230313_20230313_IEXTP1_DEEP1.0.pcap llin17@xacc-head-000-5.csl.illinois.edu:/mnt/shared/home/llin17

# Output "It is a test"
echo "It is a test"
```

The file size of test_jobsubmission.sh is 455b.

You can then submit the hob and run it on an Alveo U250 by submitting it to one of the Alveo queues. For example:

```
sbatch -p u250-run test_jobsubmission.sh
```
![img](/images/Job_submisson.png)
output:
![img](/images/output.png)

The size of test_jobsubmission.out is only 13b and the test_jobsubmission.err is 211b.


You may provide many more options to the sbatch command to determine the name of your job, the number of requested nodes, error management etc. For details, please refer to the SLURM documentation.

**NOTE: Jobs can be submitted from login nodes or the development node.** 

Once a job is complete, your output is saved to a run log named *slurm-<jobid>.out* in your home directory by default. You may change the name and location of this via the job script.

You can monitor the state of your submitted jobs via

```
squeue
```
![img](/images/Job_submisson.png)

It is possible to request an interactive job. This will log you directly into a compute node, without the need to write a job script. Once logged in, you can interact with the node and run as many commands as you’d like within the allotted time limits. Please refer to the SLURM documentation on how to run interactive jobs. As an example, to interact with the U280 nodes:

```
srun -p u280-run -n 1 --pty bash -i
```

I have upload IEX market data named 'data_feeds_20230313_20230313_IEXTP1_DEEP1.0.pcap' on HACC cluster aleady which is 28.12gb
![img](/images/IEX_market_data.png)
Also, I have my loop data script ready on HACC also 

```
#!/bin/bash
#SBATCH --job-name=loopback

#SBATCH --time=00:30:30
#SBATCH --partition=u250-short
#SBATCH --ntasks-per-node=1
#SBATCH --output=loopback.out
#SBATCH --error=loopback.err

# Load XRT
. /opt/xilinx/xrt/setup.sh

# Define the input file path
input_file=/mnt/shared/home/llin17/uploading_example/data_feeds_20230313_20230313_IEXTP1_DEEP1.0.pcap

# Loop over the first 100 lines of the input file
for ((i=1; i<=100; i++))
do
    # Read the ith line of the input file
    line=$(sed "${i}q;d" $input_file)

    # Transfer the line to the NIC using netcat
    echo $line | nc -w 1 $NIC_IP $NIC_PORT

    # Echo a message to show that the line has been transferred
    echo "Line $i transferred to $NIC_IP:$NIC_PORT"
done

# Output "It is a test"
echo "It is a test"
```
and now we can use above command and loopback.sh to loop this IEX market data.

Here is the result.
![img](/images/result1.png)
![img](/images/result2.png)

### Receive Side

To capture the network traffic on the HACC Cluster we originally were using tcpdump on a compute node to view the data coming in, however we were told by the system admins we were unable to use it anymore due to security concerns. We had started to create some simple python scripts to view incoming data to a port.
```css
import socket
from datetime import datetime
import time

UDP_IP = " "
UDP_PORT = x
BUF_SIZE = x
RUNTIME = x # Capture time in seconds

time = datetime.now()
stamp = time.strftime(%Y_%m_%d_%H_%M_%S)
fname = f"UDP_output_{stamp}.txt"

sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
sock.bind((UDP_IP, UDP_PORT))

with open(fname, 'w') as f:

start_time = time.time()
elapsed_time = 0
while elapsed_time < RUNTIME:
    data, addr = sock.recvfrom(BUF_SIZE)
    f.write(data)
    elapsed_time = time.time() - start_time
``` 


## SLURM Commands for HACC
#### Slurm Command
HACC cluster uses the the Slurm job scheduler to manage resources. Here is a [command cheat sheet](https://slurm.schedmd.com/pdfs/summary.pdf) for using slurm. Or you may refer to their [quick start user guide.](https://slurm.schedmd.com/archive/slurm-17.11.2/quickstart.html)

Job Submission:

salloc - Obtain a job allocation.
sbatch - Submit a batch script for later execution.
srun - Obtain a 1ob allocation (as needed) and execute an application

| --array=<indexes><br />(eg.”–array = 1-10:)  | Job array specification<br />(batch command only)            |
| :------------------------------------------: | :----------------------------------------------------------- |
|               --account=<name>               | Account to be charged for resources<br />used.               |
| --begin=<time><br/>(e.g. "--begin=18:00:00") | Initiate job after specified time                            |
|              --clusters=<name>               | Clusters(s) to run the job (sbatch command only)             |
|            –constraint=<feature>             | Required node features                                       |
|            -cpu-per-task=<count>             | Number of CPUs required per task.                            |
|          –dependency=<state:jobid>           | Defer job until specified jobs reach specified state.        |
|              –error=<filename>               | File in which to store job error messages.                   |
|               -exclude=<names>               | Specific host names to exclude from job allocation.          |
|              -exclusive[=user]               | Allocated nodes can not be shared with other jobs/users.     |
|            -export=<name =value>             | Export identified environment variables.                     |
|             -gres=<name:count)>              | Generic resources required per node.                         |
|                – input=<name>                | File from which to read job input data.                      |
|               –﻿job-name=<name>               | Job name.                                                    |
|                   - label                    | Prepend task ID to output(srun command only).                |
|           -licenses=<name:count >            | License resources required for entire job                    |
|                  -mem=<MB>                   | Memory required per node.                                    |
|              -mem-per-cpu=<MB>               | Memory required per allocated CPU.                           |
|           -﻿N<minnodes[-maxnodes]>            | Node count required for the job.                             |
|                  -n<count>                   | Number of tasks to be launched.                              |
|              -nodelist=<names>               | Specific host names to include in job allocation.            |
|                -output=<name>                | File in which to store job output.                           |
|             – ﻿partition=<names>              | Partition/queue in which to run the job.                     |
|                 -gos=<name>                  | Quality Of Service.                                          |
|          -signal= B: <num>[(@time]           | Signal job when approaching time limit.                      |
|                 -time=<time>                 | Wall clock time limit.                                       |
|            wrap=<command string>             | Wrap specified command in a simple "sh" shell. (sbatch command only) |
|                                              |                                                              |

Accounting:

sacct - Display accounting data.

| –allusers           | Displays all users jobs.                                     |
| ------------------- | ------------------------------------------------------------ |
| -accounts=<name>    | Displays jobs with specified accounts.                       |
| -endtime=<time>     | End of reporting period.                                     |
| -format=<spec>      | Format output.                                               |
| -name=<jobname>     | Display jobs that have any of these name(s).                 |
| -partition=<names>  | Comma separated list of partitions to select jobs and job steps from. |
| -state=<state list> | Display jobs with specified states.                          |
| starttime=<time>    | Start of reporting period.                                   |
|                     |                                                              |

sacetmgr - View and modify account information

Options:

| \- immediate | Commit changes immediately. |
| ------------ | --------------------------- |
| --parseable  | Output delimited by ‘\|’    |

Commands:

| add <ENTITY> <SPECS> create <ENTITY> <SPECS> | Add an entity. Identical to the create command. |
| -------------------------------------------- | ----------------------------------------------- |
| delete <ENTITY> where <SPECS>                | Delete the specified entities.                  |
| list <ENTITY> [<SPECS>]                      | Display information about the specific entity.  |
| modify <ENTITY> where <SPECS> set <SPECS>    | Modify an entity.                               |

Entities:

| account | Account associated with job.              |
| ------- | ----------------------------------------- |
| cluster | Cluster:Name parameter in the sturm.conf. |
| qos     | Quality of Service.                       |
| user    | User name in system.                      |

Job Management:

sbcast - Transfer file to a job's compute nodes.

sbcast [options] SOURCE DESTINATION

| --force    | Replace previously existing file.                            |
| ---------- | ------------------------------------------------------------ |
| --preserve | Preserve modification times, access times, and access permissions. |

scancel - Signal jobs, job arrays, and/or job steps.

| \- account=<name>   | Operate only on jobs charging the specified account.         |
| ------------------- | ------------------------------------------------------------ |
| -name=<name>        | Operate only on jobs with specified name                     |
| -partition=<names>  | Operate only on jobs in the specified partition/queue.       |
| -qos=<name>         | Operate only on jobs using the specified quality of service. |
| -reservation=<name> | Operate only on jobs using the specified reservation.        |
| --state=<names>     | Operate only on jobs in the specified state.                 |
| --user=<name>       | Operate only on jobs from the specified user.                |
| --nodelist=<names>  | Operate only on jobs using the specified compute nodes.      |
|                     |                                                              |

Squeue- View information about jobs.

| --account=<name>                       | View only jobs with specified accounts.                      |
| -------------------------------------- | ------------------------------------------------------------ |
| --clusters=<name>                      | View jobs on specified clusters.                             |
| --format=<spec> (e.g."_-format=%i %j") | Output format to display. Specify fields, size, order, etc.  |
| --jobs<job_id list>                    | Comma separated list of job IDs to display.                  |
| --name=<name>                          | View only jobs with specified names.                         |
| -partition=<names>                     | View only jobs in specified partitions.                      |
| --priority                             | Sort jobs by priority.                                       |
| --qos=<name>                           | View only jobs with specified Qualities Of Service.          |
| --start                                | Report the expected start time and resources to be allocated for pending jobs in order of increasing start time. |
| --state=<names>                        | View only jobs with specified states.                        |
| --Users=<names>                        | View only jobs for specified users.                          |
|                                        |                                                              |

sinfo - View information about nodes and partitions.

| --all              | Display information about all partitions.                    |
| ------------------ | ------------------------------------------------------------ |
| --dead             | If set, only report state information for non responding (dead) nodes. |
| -format=<spec>     | Output format to display.                                    |
| -iterate=<seconds> | Print the state at specified interval.                       |
| - -long            | Print more detailed information.                             |
| -﻿- Node            | Print information in a node-oriented format.                 |
| -partition=<names> | View only specified partitions.                              |
| -reservation       | Display information about advanced reservations.             |
| -R                 | Display reasons nodes are in the down, drained, fail or failing state. |
| --state=<names>    | View only nodes specified states.                            |



scontrol - Used view and modify configuration and state.

Also see the sview graphical user interface version.

| –details  | Make show command print more details. |
| --------- | ------------------------------------- |
| -oneliner | Print information on one line.        |

Commands:

| create SPECIFICATION | Create a new partition or                                    |
| -------------------- | ------------------------------------------------------------ |
| delete SPECIFICATION | Delete the entry with the specified SPECIFICATION            |
| reconfigure          | All Slurm daemons will re-read the configuration file.       |
| requeue JOB_LIST     | Requeue a running, suspended or completed batch job.         |
| show ENTITY ID       | Display the state of the specified entity with the specified identification |
| update SPECIFICATION | Update job, step, node, partition, or reservation configuration per the supplied specification. |

Environment Variables:

| SLURM_ARRAY_JOB_ID    | Set to the job ID if part of a job array   |
| --------------------- | ------------------------------------------ |
| SLURM_ARRAY_TASK_ID   | Set to the task ID if part of a job array. |
| SLURM_CLUSTER_NAME    | Name of the cluster executing the job.     |
| SLURM_CPUS_PER_TASK   | Number of CPUs requested per task          |
| SLURM_JOB_ACCOUNT     | Account name.                              |
| SLURM_JOB_ID          | Job ID.                                    |
| SLURM_JOB_NAME        | Job Name.                                  |
| SLUM_JOB_NODELIST     | Names of nodes allocated to job.           |
| SLUM_JOB_NUM_NODES    | Number of nodes allocated to job.          |
| SLURM_JOB_PARTITION   | Partition/queue running the job.           |
| SLURM_JOB_UID         | User ID of the job's owner.                |
| SLURM_JOB_USER        | User name of the job's owner.              |
| SLURM_RESTART_COUNT   | Number of times job has restarted.         |
| SLURM_PROCID          | Task ID (MPI rank).                        |
| SLURM_STEPvID         | Job step ID.                               |
| SLURM_STEP_NUM_ TASKS | Task count (number of MPI ranks).          |
|                       |                                            |

Daemons:

| slurmctld | Executes on cluster's "head" node to manage workload.        |
| --------- | ------------------------------------------------------------ |
| slurmd    | Executes on each compute node to locally manage resources.   |
| slurmdbd  | Manages database of resources limits, licenses, and archives accounting records. |
